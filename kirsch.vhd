
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity kirsch is
  port(
    ------------------------------------------
    -- main inputs and outputs
    i_clock    : in  std_logic;                      
    i_reset    : in  std_logic;                      
    i_valid    : in  std_logic;                 
    i_pixel    : in  std_logic_vector(7 downto 0);
    o_valid    : out std_logic;                 
    o_edge     : out std_logic;	                     
    o_dir      : out std_logic_vector(2 downto 0);                      
    o_mode     : out std_logic_vector(1 downto 0);
    o_row      : out std_logic_vector(7 downto 0);
    ------------------------------------------
    -- debugging inputs and outputs
    debug_key      : in  std_logic_vector( 3 downto 1) ; 
    debug_switch   : in  std_logic_vector(17 downto 0) ; 
    debug_led_red  : out std_logic_vector(17 downto 0) ; 
    debug_led_grn  : out std_logic_vector(5  downto 0) ; 
    debug_num_0    : out std_logic_vector(3 downto 0) ; 
    debug_num_1    : out std_logic_vector(3 downto 0) ; 
    debug_num_2    : out std_logic_vector(3 downto 0) ; 
    debug_num_3    : out std_logic_vector(3 downto 0) ; 
    debug_num_4    : out std_logic_vector(3 downto 0) ;
    debug_num_5    : out std_logic_vector(3 downto 0) ;
    debug_num_6    : out std_logic_vector(3 downto 0) ;
    debug_num_7    : out std_logic_vector(3 downto 0) ;
    debug_num_8    : out std_logic_vector(3 downto 0) 
    ------------------------------------------
  );  
end entity;


architecture main of kirsch is
	-- memory
	type m_output_array is array (2 downto 0) of std_logic_vector (7 downto 0);
	signal m_output : m_output_array;
	signal m_wren : std_logic_vector(2 downto 0);
	signal m_address : std_logic_vector(7 downto 0);
	signal m_row_index : std_logic_vector(2 downto 0);
	signal m_column_index : unsigned(7 downto 0);
	signal m_row_count : unsigned(7 downto 0);
	signal valid : std_logic_vector(9 downto 0);
	signal calc_ready : std_logic;
	signal receiving : std_logic;
	signal receiving2 : std_logic;
	signal receiving3 : std_logic;
	
	signal a, b, c, d, e, f, g, h, i : std_logic_vector(7 downto 0); -- convolution table
	signal r1, r2, r3, r4, r5 : unsigned(7 downto 0);
	signal r6 : unsigned(8 downto 0);
	signal r7 : unsigned(10 downto 0);
	signal r8, r9, r10, r15 : unsigned(9 downto 0);
	signal r11 : unsigned(12 downto 0);
	signal r12 : unsigned(11 downto 0);
	signal r13 : unsigned(10 downto 0);
	signal r14: unsigned(10 downto 0);
	signal m1 : unsigned(7 downto 0);
	signal m2 : unsigned(9 downto 0);
	signal a1 : unsigned(8 downto 0);
	signal a2 : unsigned(9 downto 0);
	signal a3 : unsigned(10 downto 0);
	signal a4 : unsigned(12 downto 0);
	signal a5 : unsigned(10 downto 0);
	signal s1 : signed(13 downto 0);
	signal temp_c, temp_d : std_logic_vector(7 downto 0);
	
	signal edge1, edge2, edge3 : std_logic_vector(2 downto 0);
	signal s2_edge1, s2_edge2, s2_edge3, s2_edge4 : std_logic_vector(2 downto 0);
	signal s3_edge : std_logic_vector(2 downto 0);
	
	constant W : std_logic_vector(2 downto 0) := "001";
	constant NW : std_logic_vector(2 downto 0) := "100";
	constant N : std_logic_vector(2 downto 0) := "010";
	constant NE : std_logic_vector(2 downto 0) := "110";
	constant EA : std_logic_vector(2 downto 0) := "000";
	constant SE : std_logic_vector(2 downto 0) := "101";
	constant S : std_logic_vector(2 downto 0) := "011";
	constant SW : std_logic_vector(2 downto 0) := "111";
	
	signal edgeReady : std_logic;
	signal pick_edge1 : std_logic;
	
	begin
	
	-- Memory Section --
	-- Generate 3 instances of memory.
	create_mem : for i in 0 to 2 generate
		mem : entity work.mem(main) port map (
			address => m_address,
			clock => i_clock,
			data => i_pixel,
			wren => m_wren(i),
			q => m_output(i)
		);
	end generate;
	
	process begin
		wait until rising_edge(i_clock);
		if (i_reset = '1') then
			m_row_index <= "001";
			m_column_index <= (others => '0'); -- "00000000";
			m_row_count <= (others => '0'); -- "00000000";
			receiving <= '0';
		else
			if (i_valid = '1') then
				receiving <= '1';
				if (m_column_index = 255) then
					m_row_count <= m_row_count + 1;
					if (m_row_count = 255) then
						m_row_index <= "001";
						receiving <= '0';
					else
						m_row_index <= m_row_index(1 downto 0)&m_row_index(2); -- shift bit to the left.
					end if;
				end if;	
	
				a <= b;
				h <= i;
				g <= f;
				b <= c;
				i <= d;
				f <= e;
				c <= temp_c;
				d <= temp_d;
				e <= i_pixel;
				
				m_column_index <= m_column_index + 1;
			end if;
		end if;
	end process;
	
	o_row <= std_logic_vector(m_row_count);
	
	o_mode(1) <= not i_reset;
	o_mode(0) <= i_reset or receiving or receiving2 or receiving3;
	
	m_address <= std_logic_vector(m_column_index);
	m_wren <= m_row_index when i_valid = '1' else "000";
	temp_c <= m_output(0) when m_row_index(2) = '1' else m_output(1) when m_row_index(0) = '1' else m_output(2);
	temp_d <= m_output(1) when m_row_index(2) = '1' else m_output(2) when m_row_index(0) = '1' else m_output(0);
	
	a1 <= ("0" & r1) + ("0" & r2);
	a2 <= ("00" & r5) + ("0" & r6);
	a3 <= ("0" & r6)+ r7;
	m1 <= r3 when r3 >= r4 else r4;
	
	a4 <= ("0" & r12) + r13;
	a5 <= r13 + r14;
	m2 <= r10 when r10 >= r11(9 downto 0) else r11(9 downto 0);
	s1 <= signed(("0" & (r15&"000")) - ("0" & r11));
	o_edge <= edgeReady;
	o_dir <= s3_edge when edgeReady = '1' else "000";
	o_valid <= valid(9);
	
	-- VALID BITS CONTROL --	
	calc_ready <= '1' when (m_row_count > 1 and m_column_index > 1) else '0';
	valid(0) <= (calc_ready and i_valid) when i_reset = '0' else '0';
	
	for_v : for i in 1 to 9 generate
	process begin
		wait until rising_edge(i_clock);
		if (i_reset = '1') then
			valid(i) <= '0';
		else
			valid(i) <= valid(i - 1);
		end if;
	end process;
	end generate;
	
	-- STAGE 1 --
	process begin
		wait until rising_edge(i_clock);
		if valid(0) = '1' then
			r1 <= unsigned(i);
			r2 <= unsigned(b);
			r3 <= unsigned(f);
			r4 <= unsigned(c);
		elsif valid(1) = '1' then
			r1 <= unsigned(c);
			r2 <= r4;
			r3 <= unsigned(d);
			r4 <= r2;
			r5 <= m1;
			r6 <= a1;
			r7 <= "00" & r6;
			r8 <= a2;
			r9 <= r8;
		elsif valid(2) = '1' then
			r1 <= unsigned(e);
			r2 <= r3;
			r3 <= unsigned(f);
			r4 <= r1;
			r5 <= m1;
			r6 <= a1;
			r7 <= "00" & r6;
			r8 <= a2;
			r9 <= r8;
		elsif valid(3) = '1' then
			r1 <= unsigned(g);
			r2 <= r3;
			r3 <= unsigned(h);
			r4 <= r1;
			r5 <= m1;
			r6 <= a1;
			r7 <= a3;
			r8 <= a2;
			r9 <= r8;
		end if;
	end process;
	
	-- Find edge direction.
	process begin
		wait until rising_edge(i_clock);
		if valid(1) = '1' then
			if r3 >= r4 then
				edge1 <= W;
			else
				edge1 <= NW;
			end if;
		elsif valid(2) = '1' then
			if r3 > r4 then
				edge2 <= NE;
			else
				edge2 <= N;
			end if;
		elsif valid(3) = '1' then
			if r3 > r4 then
				edge3 <= SE;
			else
				edge3 <= EA;
			end if;
		end if;
		
	end process;
	
	process begin
		wait until rising_edge(i_clock);
		if valid(4) = '1' then
			if r3 > r4 then
				s2_edge4 <= SW;
			else
				s2_edge4 <= S;
			end if;
			s2_edge1 <= edge1;
			s2_edge2 <= edge2;
			s2_edge3 <= edge3;
		elsif valid(5) = '1' then
			if r10 >= r11 then
				pick_edge1 <= '1';
			else
				pick_edge1 <= '0';
			end if;
		elsif valid(6) = '1' then
			if r10 >= r11 then
				pick_edge1 <= '1';
			else
				pick_edge1 <= '0';
			end if;
			
			if pick_edge1 = '1' then
				s2_edge1 <= s2_edge1;
			else
				s2_edge1 <= s2_edge2;
			end if;
		elsif valid(7) = '1' then
			if r10 >= r11 then
				pick_edge1 <= '1';
			else
				pick_edge1 <= '0';
			end if;
			
			if pick_edge1 = '1' then
				s2_edge3 <= s2_edge3;
			else
				s2_edge3 <= s2_edge4;
			end if;
		end if;
	end process;
	
	-- STAGE 2 --
	process begin
		wait until rising_edge(i_clock);
		if (i_reset = '1') then
			receiving2 <= '0';
		else
			if valid(4) = '1' then
				r10 <= r9;
				r11 <= "000" & r8;
				r12 <= "0000" & m1;
				r13 <= "00" & a1;
				r14 <= a3;
				r15 <= a2;
				receiving2 <= receiving;
			elsif valid(5) = '1' then
				r10 <= r15;
				r11 <= a4;
				r12 <= "0000" & m1;
				r13 <= a5;
				r14 <= a3;
				r15 <= m2;
			elsif valid(6) = '1' then
				r10 <= r15;
				r11 <= "000" & m2;
				r12 <= r13(10 downto 0)&"0";
				r13 <= r13;
				r14 <= a3;
				r15 <= m2;
			elsif valid(7) = '1' then
				r10 <= r15;
				r11 <= a4;
				r12 <= "0000" & m1;
				r13 <= "00" & a1;
				r14 <= a3;
				r15 <= m2;
			end if;
		end if;
	end process;
	
	process begin
		wait until rising_edge(i_clock);
		if (i_reset = '1') then
			receiving3 <= '0';
		else
			if valid(8) = '1' then
				if s1(13 downto 9) >= 1 or s1(8 downto 7) = "11" then
					edgeReady <= '1';
				else
					edgeReady <= '0';
				end if;
				
				if pick_edge1 = '1' then
					s3_edge <= s2_edge1;
				else
					s3_edge <= s2_edge3;
				end if;
				--s3_edge <= s2_edge1;
				receiving3 <= receiving2;
			end if;
		end if;
	end process;
  
end architecture;
